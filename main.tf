# configure aws provider
provider "aws" {
  region = var.region
}


# #State file to s3
# terraform {
#   backend "s3" {
#     bucket = "terraform-agrisathi"
#     key    = "dev/terraform.tfstate"
#     region = "ap-south-1"
#   }
# }


#Create VPC
module "vpc" {
  source                  = "./modules/vpc"
  project_name            = var.project_name
  env                     = var.env
  vpc_cidr                = var.vpc_cidr
  public_subnet_az1_cidr  = var.public_subnet_az1_cidr
  public_subnet_az2_cidr  = var.public_subnet_az2_cidr
  private_subnet_az1_cidr = var.private_subnet_az1_cidr
  private_subnet_az2_cidr = var.private_subnet_az2_cidr
}

# create EC2
module "ec2" {
  source                = "./modules/ec2"
  project_name          = var.project_name
  env                   = var.env
  subnet_id             = module.vpc.public_subnet_az1
  ec2_ami               = var.ec2_ami
  aws_eip_id            = module.vpc.aws_eip_id
  ec2_type              = var.ec2_type
  ec2_security_group_id = module.vpc.ec2_security_group_id
  key_name              = var.key_name
  depends_on = [
    module.vpc
  ]
}

# #Create RDS
module "rds" {
  source                = "./modules/rds"
  project_name          = var.project_name
  env                   = var.env
  rds_name              = var.rds_name
  subnet_id_az1         = module.vpc.private_subnet_az1
  subnet_id_az2         = module.vpc.public_subnet_az2
  allocated_storage     = var.allocated_storage
  engine                = var.db_engine
  engine_version        = var.engine_version
  instance_class        = var.instance_class
  rds_security_group_id = module.vpc.rds_security_group
  username              = var.username
  password              = var.password
  skip_final_snapshot   = var.skip_final_snapshot
  depends_on = [
    module.vpc
  ]
}


# # Route 53
# module "route53" {
#   source      = "./modules/route53"
#   domain_name = var.domain_name
# }

# #S3 bucket 
# module "s3" {
#   source      = "./modules/s3"
#   bucket_name = var.bucket_name
#   bucket_tag  = var.bucket_tag
#   oai_ima_arn = module.cloudfront.oai
# }
# # Cert Manager
# module "certmanager" {
#   source           = "./modules/certmanager"
#   wild_domain_name = var.wild_domain_name
#   w_domain_name    = var.w_domain_name
#   cert_managertag  = var.cert_managertag
#   hosted_zone_id   = module.route53.hosted_zone_id
# }
# ### CloudFront
# module "cloudfront" {
#   source                  = "./modules/cloudfront"
#   bucket_domain_name      = module.s3.bucket_domain_name
#   bucket_arn              = module.s3.arn
#   bucket_id               = module.s3.bucket_id
#   aws_acm_certificate_arn = module.certmanager.acm_certificate_arn
#   cname_1                 = var.cname_1
#   cname_2                 = var.cname_2
#   depends_on = [
#     module.certmanager
#   ]
# }

# ### CloudFront
# module "route53_records" {
#   source                = "./modules/route53_records"
#   hosted_zone_id        = module.route53.hosted_zone_id
#   cloudfront_DNS_domain = module.cloudfront.cloudfront_DNS_domain
#   cloudfront_id         = module.cloudfront.cloudfront_id
#   domain_name           = var.domain_name
#   depends_on = [
#     module.certmanager
#   ]
# }
