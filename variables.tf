
#Create VPC
variable "project_name" {
  type    = string
  default = "crowdinnio"
}
variable "env" {
  type    = string
  default = "dev"
}
variable "region" {
  type    = string
  default = "us-east-1"
}

variable "vpc_cidr" {
  type    = string
  default = "10.0.0.0/16"
}

variable "public_subnet_az1_cidr" {
  type    = string
  default = "10.0.0.0/20"
}


variable "public_subnet_az2_cidr" {
  type    = string
  default = "10.0.16.0/20"
}

variable "private_subnet_az1_cidr" {
  type    = string
  default = "10.0.128.0/20"
}


variable "private_subnet_az2_cidr" {
  type    = string
  default = "10.0.144.0/20"
}

# EC2
variable "ec2_ami" {
  type    = string
  default = "ami-08d4ac5b634553e16"
}

variable "ec2_type" {
  type    = string
  default = "t2.micro"
}

variable "key_name" {
  type    = string
  default = "e-mykey"
}


# Create RDS
variable "rds_name" {
  type    = string
  default = "crowdinnio"
}

variable "allocated_storage" {
  type    = string
  default = "20"
}

variable "db_engine" {
  type    = string
  default = "postgres"
}

variable "engine_version" {
  type    = string
  default = "14.3"
}

variable "instance_class" {
  type    = string
  default = "db.t3.micro"
}

variable "username" {
  type    = string
  default = "crowdinnio"
}

variable "password" {
  type    = string
  default = "admin1234"
}

variable "skip_final_snapshot" {
  type    = bool
  default = "true"
}

#S3 Bucket
variable "bucket_name" {
  type    = string
  default = "agrisathi.com"
}

variable "bucket_tag" {
  type    = string
  default = "agrisathi.com"
}

# Cert Manager
variable "wild_domain_name" {
  type    = string
  default = "*.agrisathi.com"
}
variable "w_domain_name" {
  type    = string
  default = "agrisathi.com"
}
variable "cert_managertag" {
  type    = string
  default = "agrisathi.com-certmanager"
}
# Route 53
variable "domain_name" {
  type    = string
  default = "agrisathi.com"
}

# CloudFront
variable "cname_1" {
  type    = string
  default = "agrisathi.com"
}

variable "cname_2" {
  type    = string
  default = "www.agrisathi.com"
}
