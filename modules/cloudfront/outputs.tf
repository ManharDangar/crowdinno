output "oai" {
  value = aws_cloudfront_origin_access_identity.oai.iam_arn
}

output "cloudfront_DNS_domain" {
  value = aws_cloudfront_distribution.s3_distribution.domain_name
}

output "cloudfront_id" {
  value = aws_cloudfront_distribution.s3_distribution.hosted_zone_id
}