variable "bucket_domain_name" {}
variable "bucket_arn" {}
variable "bucket_id" {}
variable "aws_acm_certificate_arn" {}
variable "cname_1" {}
variable "cname_2" {}