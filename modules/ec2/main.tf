resource "aws_eip_association" "eip_assoc" {
  instance_id   = aws_instance.web.id
  allocation_id = var.aws_eip_id
}

resource "aws_instance" "web" {
  ami           = var.ec2_ami
  instance_type = var.ec2_type
  subnet_id =  var.subnet_id
  user_data = file("${path.module}/user-data.sh")
  vpc_security_group_ids = [var.ec2_security_group_id]
  key_name= var.key_name
  tags = {
    Name = "${var.project_name}-${var.env}"
  }
}