# terraform aws db subnet group
resource "aws_db_subnet_group" "database-subnet-group" {
  name         = "${var.project_name}-subnet-group-${var.env}"
  subnet_ids   = [var.subnet_id_az1, var.subnet_id_az2]
  tags   = {
    Name = "${var.project_name}-subnet-group-${var.env}"
  }
}

resource "aws_db_instance" "rds" { 
  allocated_storage    = var.allocated_storage
  engine               = var.engine
  engine_version       = var.engine_version
  instance_class       = var.instance_class
  identifier           = "${var.rds_name}-${var.env}"
  db_name              = var.rds_name
  username             = var.username
  password             = var.password
  vpc_security_group_ids = [var.rds_security_group_id]
  db_subnet_group_name = aws_db_subnet_group.database-subnet-group.id
  skip_final_snapshot  = var.skip_final_snapshot
}

