# create vpc
resource "aws_vpc" "vpc" {
  cidr_block              = var.vpc_cidr
  instance_tenancy        = "default"
  enable_dns_hostnames    = true

  tags      = {
    Name    = "${var.project_name}-vpc-${var.env}"
  }
}

# create internet gateway and attach it to vpc
resource "aws_internet_gateway" "internet_gateway" {
  vpc_id    = aws_vpc.vpc.id
  tags      = {
    Name    = "${var.project_name}-ig-${var.env}"
  }
}


# use data source to get all avalablility zones in region
data "aws_availability_zones" "available_zones" {}

# create public subnet az1
resource "aws_subnet" "public_subnet_az1" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.public_subnet_az1_cidr
  availability_zone       = data.aws_availability_zones.available_zones.names[0]
  map_public_ip_on_launch = true

  tags      = {
    Name    = "${var.project_name}-pub_subnet_az1-${var.env}"
  }
}

# create public subnet az2
resource "aws_subnet" "public_subnet_az2" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.public_subnet_az2_cidr
  availability_zone       = data.aws_availability_zones.available_zones.names[1]
  map_public_ip_on_launch = true

  tags      = {
    Name    = "${var.project_name}-pub_subnet_az2-${var.env}"
  }
}

# create route table and add public route
resource "aws_route_table" "public_route_table" {
  vpc_id       = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internet_gateway.id
  }

  tags       = {
    Name     = "${var.project_name}-public-route-table-${var.env}"
  }
}

# associate public subnet az1 to "public route table"
resource "aws_route_table_association" "public_subnet_az1_route_table_association" {
  subnet_id           = aws_subnet.public_subnet_az1.id
  route_table_id      = aws_route_table.public_route_table.id
}

# associate public subnet az2 to "public route table"
resource "aws_route_table_association" "public_subnet_az2_route_table_association" {
  subnet_id           = aws_subnet.public_subnet_az2.id
  route_table_id      = aws_route_table.public_route_table.id
}

# create private app subnet az1
resource "aws_subnet" "private_app_subnet_az1" {
  vpc_id                   = aws_vpc.vpc.id
  cidr_block               = var.private_subnet_az1_cidr
  availability_zone        = data.aws_availability_zones.available_zones.names[0]
  map_public_ip_on_launch  = false

  tags      = {
    Name    = "${var.project_name}-private subnet az1-${var.env}"
  }
}

# create private app subnet az2
resource "aws_subnet" "private_app_subnet_az2" {
  vpc_id                   = aws_vpc.vpc.id
  cidr_block               = var.private_subnet_az2_cidr
  availability_zone        = data.aws_availability_zones.available_zones.names[1]
  map_public_ip_on_launch  = false

  tags      = {
    Name    = "${var.project_name}-private subnet az2-${var.env}"
  }
}

 resource "aws_eip" "aws_eip_for_nat" {
   vpc   = true
   tags      = {
   Name    = "${var.project_name}-eip-for-nat-${var.env}"
   }
 }

#Elastic ip
 resource "aws_eip" "nateIP" {
   vpc   = true
    tags      = {
    Name    = "${var.project_name}-eip-for-ec2-${var.env}"
  }
 }

 

#NAT Gateway for VPC
resource "aws_nat_gateway" "natgw" {
  allocation_id = aws_eip.aws_eip_for_nat.id
  subnet_id = aws_subnet.public_subnet_az1.id
  tags = {
    Name = "${var.project_name}-natgw-public-subnet-${var.env}"
  }
}

# create route table and add Private route
resource "aws_route_table" "private_route_table" {
  vpc_id       = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.natgw.id
  }

  tags       = {
    Name     = "${var.project_name}-private-route-table-${var.env}"
  }
}

# associat Private subnet az1 to "private route table"
resource "aws_route_table_association" "private_subnet_az1_route_table_association" {
  subnet_id           = aws_subnet.private_app_subnet_az1.id
  route_table_id      = aws_route_table.private_route_table.id
}

# associat Private subnet az2 to "private route table"
resource "aws_route_table_association" "private_subnet_az2_route_table_association" {
  subnet_id           = aws_subnet.private_app_subnet_az2.id
  route_table_id      = aws_route_table.private_route_table.id
}


# create security group for EC2
resource "aws_security_group" "ec2_security_group" {
  name        = "${var.project_name}-ec2-sg-${var.env}"
  description = "enable http/https access on port 80/443 & dynamodb 3306"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    description      = "http access"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "https access"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

    ingress {
    description      = "SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags   = {
    Name = "${var.project_name}-ec2-sg-${var.env}"
  }
}


# create security group for RDS
resource "aws_security_group" "rds_security_group" {
  name        = "${var.project_name}-rds-sg-${var.env}"
  description = "rds-postgresql-sg"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    description      = "postgresql access"
    from_port        = 5432
    to_port          = 5432
    protocol         = "tcp"
    security_groups = [aws_security_group.ec2_security_group.id]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags   = {
    Name = "${var.project_name}-rds-postgresql-sg${var.env}"
  }
}