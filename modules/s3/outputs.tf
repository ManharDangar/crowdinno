output "bucket_domain_name" {
  value = aws_s3_bucket.b.bucket_regional_domain_name
}

output "bucket_id" {
  value = aws_s3_bucket.b.id
}

output "arn" {
  value = aws_s3_bucket.b.arn
}